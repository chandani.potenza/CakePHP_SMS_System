<br />
<div class="index large-4 medium-4 large-offset-4 medium-offset-4 columns">
 <div class="panel">
    <?php // echo $this->Flash->render('auth'); ?>
    <h1 class="text-center">LOGIN</h1>
      <?= $this->Form->create(); ?>
      <legend><?= __('Please enter your username and password') ?></legend>
       <?php
            echo  $this->Form->input('username');
            echo  $this->Form->input('password',array('type' => 'password'));
        ?>
       <?= $this->Form->submit('SignIn',array('class' => 'button')); ?>
       <?= $this->Html->link('SignUp',['controller' => 'users','action' => 'add']); ?>
       <?= $this->Form->end(); ?>
 </div>
</div>



